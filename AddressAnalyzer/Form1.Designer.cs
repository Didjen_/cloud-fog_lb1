﻿using System.Windows.Forms;

namespace AddressAnalyzer
{
  partial class Form1
  {
    /// <summary>
    ///  Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    ///  Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }

      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.tlp_Grid = new System.Windows.Forms.TableLayoutPanel();
      this.tlp_Nuds = new System.Windows.Forms.TableLayoutPanel();
      this.tpl_Statistics = new System.Windows.Forms.TableLayoutPanel();
      this.btn_Add = new System.Windows.Forms.Button();
      this.btn_find = new System.Windows.Forms.Button();
      this.btn_LoadFromFile = new System.Windows.Forms.Button();
      this.panel = new System.Windows.Forms.Panel();
      this.btn_Export = new System.Windows.Forms.Button();
      this.b_Clear = new System.Windows.Forms.Button();
      this.panel.SuspendLayout();
      this.SuspendLayout();
      // 
      // tlp_Grid
      // 
      this.tlp_Grid.ColumnCount = 9;
      this.tlp_Grid.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
      this.tlp_Grid.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
      this.tlp_Grid.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
      this.tlp_Grid.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
      this.tlp_Grid.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
      this.tlp_Grid.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
      this.tlp_Grid.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
      this.tlp_Grid.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
      this.tlp_Grid.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
      this.tlp_Grid.Location = new System.Drawing.Point(12, 20);
      this.tlp_Grid.Name = "tlp_Grid";
      this.tlp_Grid.RowCount = 9;
      this.tlp_Grid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
      this.tlp_Grid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
      this.tlp_Grid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
      this.tlp_Grid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
      this.tlp_Grid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
      this.tlp_Grid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
      this.tlp_Grid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
      this.tlp_Grid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
      this.tlp_Grid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
      this.tlp_Grid.Size = new System.Drawing.Size(404, 230);
      this.tlp_Grid.TabIndex = 0;
      // 
      // tlp_Nuds
      // 
      this.tlp_Nuds.ColumnCount = 5;
      this.tlp_Nuds.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
      this.tlp_Nuds.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
      this.tlp_Nuds.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
      this.tlp_Nuds.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
      this.tlp_Nuds.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
      this.tlp_Nuds.Location = new System.Drawing.Point(13, 360);
      this.tlp_Nuds.Name = "tlp_Nuds";
      this.tlp_Nuds.RowCount = 2;
      this.tlp_Nuds.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.tlp_Nuds.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.tlp_Nuds.Size = new System.Drawing.Size(305, 44);
      this.tlp_Nuds.TabIndex = 6;
      // 
      // tpl_Statistics
      // 
      this.tpl_Statistics.AutoSize = true;
      this.tpl_Statistics.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
      this.tpl_Statistics.BackColor = System.Drawing.SystemColors.Window;
      this.tpl_Statistics.ColumnCount = 2;
      this.tpl_Statistics.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 75F));
      this.tpl_Statistics.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
      this.tpl_Statistics.Dock = System.Windows.Forms.DockStyle.Top;
      this.tpl_Statistics.Location = new System.Drawing.Point(0, 0);
      this.tpl_Statistics.Name = "tpl_Statistics";
      this.tpl_Statistics.RowCount = 1;
      this.tpl_Statistics.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.tpl_Statistics.Size = new System.Drawing.Size(305, 0);
      this.tpl_Statistics.TabIndex = 7;
      // 
      // btn_Add
      // 
      this.btn_Add.Location = new System.Drawing.Point(324, 284);
      this.btn_Add.Name = "btn_Add";
      this.btn_Add.Size = new System.Drawing.Size(142, 30);
      this.btn_Add.TabIndex = 8;
      this.btn_Add.Text = "Add to set";
      this.btn_Add.UseVisualStyleBackColor = true;
      this.btn_Add.Click += new System.EventHandler(this.btn_Add_Click);
      // 
      // btn_find
      // 
      this.btn_find.Location = new System.Drawing.Point(324, 356);
      this.btn_find.Name = "btn_find";
      this.btn_find.Size = new System.Drawing.Size(142, 30);
      this.btn_find.TabIndex = 9;
      this.btn_find.Text = "Find";
      this.btn_find.UseVisualStyleBackColor = true;
      this.btn_find.Click += new System.EventHandler(this.btn_find_Click);
      // 
      // btn_LoadFromFile
      // 
      this.btn_LoadFromFile.Location = new System.Drawing.Point(324, 320);
      this.btn_LoadFromFile.Name = "btn_LoadFromFile";
      this.btn_LoadFromFile.Size = new System.Drawing.Size(142, 30);
      this.btn_LoadFromFile.TabIndex = 10;
      this.btn_LoadFromFile.Text = "Load from file";
      this.btn_LoadFromFile.UseVisualStyleBackColor = true;
      this.btn_LoadFromFile.Click += new System.EventHandler(this.btn_LoadFromFile_Click);
      // 
      // panel
      // 
      this.panel.AutoScroll = true;
      this.panel.Controls.Add(this.tpl_Statistics);
      this.panel.Location = new System.Drawing.Point(472, 34);
      this.panel.Name = "panel";
      this.panel.Size = new System.Drawing.Size(305, 370);
      this.panel.TabIndex = 11;
      // 
      // btn_Export
      // 
      this.btn_Export.Location = new System.Drawing.Point(324, 392);
      this.btn_Export.Name = "btn_Export";
      this.btn_Export.Size = new System.Drawing.Size(142, 30);
      this.btn_Export.TabIndex = 12;
      this.btn_Export.Text = "Export to file";
      this.btn_Export.UseVisualStyleBackColor = true;
      this.btn_Export.Click += new System.EventHandler(this.btn_Export_Click);
      // 
      // b_Clear
      // 
      this.b_Clear.Location = new System.Drawing.Point(13, 256);
      this.b_Clear.Name = "b_Clear";
      this.b_Clear.Size = new System.Drawing.Size(142, 30);
      this.b_Clear.TabIndex = 13;
      this.b_Clear.Text = "Clear grid";
      this.b_Clear.UseVisualStyleBackColor = true;
      this.b_Clear.Click += new System.EventHandler(this.b_Clear_Click);
      // 
      // Form1
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.AutoSize = true;
      this.BackColor = System.Drawing.SystemColors.Control;
      this.ClientSize = new System.Drawing.Size(800, 450);
      this.Controls.Add(this.b_Clear);
      this.Controls.Add(this.btn_Export);
      this.Controls.Add(this.panel);
      this.Controls.Add(this.btn_LoadFromFile);
      this.Controls.Add(this.btn_find);
      this.Controls.Add(this.btn_Add);
      this.Controls.Add(this.tlp_Nuds);
      this.Controls.Add(this.tlp_Grid);
      this.Name = "Form1";
      this.Text = "Form1";
      this.Load += new System.EventHandler(this.Form1_Load);
      this.panel.ResumeLayout(false);
      this.panel.PerformLayout();
      this.ResumeLayout(false);
    }

    private System.Windows.Forms.Button b_Clear;

    private System.Windows.Forms.Button btn_Export;

    private System.Windows.Forms.Panel panel;

    private System.Windows.Forms.Button btn_Add;
    private System.Windows.Forms.Button btn_find;
    private System.Windows.Forms.Button btn_LoadFromFile;

    private System.Windows.Forms.TableLayoutPanel tpl_Statistics;

    private System.Windows.Forms.TableLayoutPanel tlp_Nuds;

    private System.Windows.Forms.TableLayoutPanel tlp_Grid;

    #endregion
  }
}