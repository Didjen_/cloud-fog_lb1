﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AddressAnalyzer.Models;
using AddressAnalyzer.Services;

namespace AddressAnalyzer
{
  public partial class Form1 : Form
  {
    private readonly TableControl _tableControl = new TableControl();
    private readonly AddressPartsControl _partsControl = new AddressPartsControl();
    private readonly AddressListControl _addressListControl = new AddressListControl();

    public Form1()
    {
      InitializeComponent();
    }


    private void Form1_Load(object sender, EventArgs e)
    {
      _tableControl.InitTable(tlp_Grid, TableCellClick);
      _partsControl.Init(tlp_Nuds, tb_ValueChanged);
      _addressListControl.Init(tpl_Statistics);
    }

    private void tb_ValueChanged(object sender, EventArgs e)
    {
      SelectPartsOnGrid();
      _addressListControl.SelectRow(null);
    }

    private void SelectPartsOnGrid()
    {
      var positions = _partsControl.GetAllValues()
        .Where(v => v != -1)
        .Select(v => new Point(v / 10, v % 10))
        .ToArray();
      _tableControl.ClearGridAndSelectCellsAtPosition(positions);
    }

    private void btn_Add_Click(object sender, EventArgs e)
    {
      var allParts = _partsControl.GetAllValues();

      if (allParts.Contains(-1))
      {
        MessageBox.Show("There are empty address parts positions");
        return;
      }

      if (allParts.Count != allParts.Distinct().Count())
      {
        MessageBox.Show("There are duplicates in address parts positions");
        return;
      }

      _addressListControl.Add(allParts);
      Refresh();
    }

    private void btn_LoadFromFile_Click(object sender, EventArgs e)
    {
      var filePath = OpenFileDialogWindow();
      if (filePath is null)
      {
        MessageBox.Show("Error while getting file path");
        return;
      }

      List<AnalyzeModel> data;
      try
      {
        data = FileLoader.LoadFromFile(filePath);
      }
      catch
      {
        MessageBox.Show("File data is invalid");
        return;
      }

      _addressListControl.AddRange(data);
    }

    private void btn_find_Click(object sender, EventArgs e)
    {
      var allParts = _partsControl.GetAllValues();
      if (allParts.All(p => p == -1))
      {
        MessageBox.Show("All address parts positions are empty");
        return;
      }

      if (allParts.All(p => p != -1))
      {
        MessageBox.Show("There is no empty address part position");
        return;
      }

      if (allParts.Where(p => p != -1).GroupBy(p => p).Any(p => p.Count() != 1))
      {
        MessageBox.Show("There are duplicates in address parts positions");
        return;
      }

      var allAddress = _addressListControl.GetAll();
      var result = AddressPartsAnalyzer.FindAbsentPart(allAddress, allParts);
      if (result is null)
      {
        MessageBox.Show("No matching pattern address found");
        return;
      }

      _addressListControl.SelectRow(result);
    }

    private void TableCellClick(object sender, EventArgs e)
    {
      if (sender is Label label)
      {
        _partsControl.SwapPartState(label.Text);
        SelectPartsOnGrid();
        _addressListControl.SelectRow(null);
      }
    }

    private void btn_Export_Click(object sender, EventArgs e)
    {
      var filePath = SaveFileDialogWindow();
      var data = _addressListControl.GetAll();
      try
      {
        FileLoader.ExportToFile(filePath, data);
      }
      catch
      {
        MessageBox.Show("Oops, error occured while exporting data");
      }
    }

    private void b_Clear_Click(object sender, EventArgs e)
    {
      _partsControl.ClearAll();
      SelectPartsOnGrid();
    }

    private string OpenFileDialogWindow()
    {
      var openFileDialog1 = new OpenFileDialog
      {
        InitialDirectory = @".\",
        Title = "Browse File",

        CheckFileExists = true,
        CheckPathExists = true,

        DefaultExt = "json",
        Filter = "Text files (*.txt)|*.txt|Json files (*.json)|*.json",
        FilterIndex = 2,
        RestoreDirectory = true,

        ReadOnlyChecked = true,
        ShowReadOnly = true
      };

      if (openFileDialog1.ShowDialog() == DialogResult.OK)
      {
        return openFileDialog1.FileName;
      }

      return null;
    }

    private string SaveFileDialogWindow()
    {
      var openFileDialog1 = new SaveFileDialog
      {
        InitialDirectory = @".\",
        Title = "Browse File",

        CheckFileExists = false,
        CheckPathExists = true,

        DefaultExt = "json",
        Filter = "Text files (*.txt)|*.txt|Json files (*.json)|*.json",
        FilterIndex = 2,
        RestoreDirectory = true,
      };

      if (openFileDialog1.ShowDialog() == DialogResult.OK)
      {
        return openFileDialog1.FileName;
      }

      return null;
    }
  }
}