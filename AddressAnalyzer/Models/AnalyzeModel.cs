namespace AddressAnalyzer.Models
{
  public class AnalyzeModel
  {
    public AnalyzeModel(int[] addressParts, int count)
    {
      AddressParts = addressParts;
      Count = count;
    }

    public int[] AddressParts { get; set; }
    public int Count { get; set; }
  }
}