using System.Drawing;

namespace AddressAnalyzer.Models
{
  public static class StaticData
  {
    public static Color DefaultColor = Color.White;
    public static Color SelectedColor = Color.Chocolate;

    public static readonly int TableRows = 9;
    public static readonly int TableColumns = 9;

    public static readonly int AddressPartsCount = 5;
  }
}