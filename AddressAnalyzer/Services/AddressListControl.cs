using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using AddressAnalyzer.Models;

namespace AddressAnalyzer.Services
{
  public class AddressListControl
  {
    private TableLayoutPanel _parentLayoutPanel;
    private readonly Label[] _headerLabels = {MakeDefaultLabel("Address"), MakeDefaultLabel("Count")};

    public List<Label[]> AllAddress { get; set; } = new List<Label[]>();

    public void Init(TableLayoutPanel tlp)
    {
      _parentLayoutPanel = tlp;
      _parentLayoutPanel.Controls.Add(_headerLabels[0], 0, 0);
      _parentLayoutPanel.Controls.Add(_headerLabels[1], 1, 0);
    }

    public void Add(IEnumerable<int> address)
    {
      var addressStr = AddressPartsToString(address);
      foreach (var tuple in AllAddress)
      {
        if (tuple[0].Text == addressStr)
        {
          tuple[1].Text = (int.Parse(tuple[1].Text) + 1).ToString();
          return;
        }
      }

      AddNewAddress(addressStr, 1);
    }

    public void AddRange(List<AnalyzeModel> models)
    {
      var distinctModels = models.GroupBy(m => m.AddressParts)
        .Select(m => new AnalyzeModel(m.Key, m.Select(v => v.Count).Sum()))
        .ToList();
      foreach (var model in distinctModels)
      {
        var addressStr = AddressPartsToString(model.AddressParts);
        if (AllAddress.Any(t => t[0].Text == addressStr))
        {
          var tuple = AllAddress.Find(t => t[0].Text == addressStr);
          tuple[1].Text = (int.Parse(tuple[1].Text) + model.Count).ToString();
        }
        else
        {
          AddNewAddress(addressStr, model.Count);
        }
      }
    }

    public List<AnalyzeModel> GetAll()
    {
      return AllAddress.Select(a => new AnalyzeModel(AddressToArray(a[0].Text), int.Parse(a[1].Text)))
        .ToList();
    }

    public void SelectRow(int[] address)
    {
      var addressStr = address is null ? String.Empty : AddressPartsToString(address);
      for (var i = 0; i < AllAddress.Count; i++)
      {
        if (AllAddress[i][0].Text == addressStr)
        {
          AllAddress[i][0].BackColor = StaticData.SelectedColor;
          AllAddress[i][1].BackColor = StaticData.SelectedColor;
        }
        else
        {
          AllAddress[i][0].BackColor = StaticData.DefaultColor;
          AllAddress[i][1].BackColor = StaticData.DefaultColor;
        }
      }
    }

    private void AddNewAddress(string address, int count)
    {
      AddTableRow();
      var addressTuple = new[] {MakeDefaultLabel(address), MakeDefaultLabel(count.ToString())};
      AllAddress.Add(addressTuple);
      var row = _parentLayoutPanel.RowCount - 1; //AllAddress.Count + 1; // offset for headers
      _parentLayoutPanel.Controls.Add(addressTuple[0], 0, row);
      _parentLayoutPanel.Controls.Add(addressTuple[1], 1, row);
    }

    private void AddTableRow()
    {
      _parentLayoutPanel.RowCount = _parentLayoutPanel.RowCount + 1;
      _parentLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Absolute, 30F));
    }

    private static string AddressPartsToString(IEnumerable<int> addressParts)
    {
      return string.Join(" : ", addressParts);
    }

    private static int[] AddressToArray(string str)
    {
      return str.Split(" :".ToArray(), StringSplitOptions.RemoveEmptyEntries)
        .Select(int.Parse)
        .ToArray();
    }

    private static Label MakeDefaultLabel(string text)
    {
      return new Label
      {
        Text = text,
        TextAlign = ContentAlignment.MiddleCenter,
        AutoSize = true
      };
    }
  }
}