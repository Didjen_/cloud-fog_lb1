using System;
using System.Collections.Generic;
using System.Linq;
using AddressAnalyzer.Models;

namespace AddressAnalyzer.Services
{
  public static class AddressPartsAnalyzer
  {
    public static int[] FindAbsentPart(List<AnalyzeModel> analyzeModels,IReadOnlyList<int> controlParts)
    {
      return analyzeModels.Where(m => IsSimilarSignature(m.AddressParts, controlParts))
        .OrderByDescending(m => m.Count)
        .FirstOrDefault()?.AddressParts;
    }

    private static bool IsSimilarSignature(IReadOnlyList<int> first, IReadOnlyList<int> second)
    {
      if (first.Count != second.Count)
      {
        throw new ArgumentException("Arguments lengths don't equal");
      }

      for (var i = 0; i < first.Count; i++)
      {
        if (first[i] != second[i] && first[i] != -1 && second[i] != -1)
        {
          return false;
        }
      }

      return true;
    }
  }
}