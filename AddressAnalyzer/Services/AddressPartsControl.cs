using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using AddressAnalyzer.Models;

namespace AddressAnalyzer.Services
{
  public class AddressPartsControl
  {
    private readonly TextBox[] _partsTextBoxes = new TextBox[StaticData.AddressPartsCount];

    public void Init(TableLayoutPanel tlp, KeyEventHandler valueChanged)
    {
      for (var i = 0; i < 5; i++)
      {
        var tb = MakeDefaultTextBox(valueChanged);
        tlp.Controls.Add(MakeDefaultLabel((i + 1).ToString()), i, 0);
        tlp.Controls.Add(tb, i, 1);
        _partsTextBoxes[i] = tb;
      }
    }

    public List<int> GetAllValues()
    {
      return _partsTextBoxes.Select(n => string.IsNullOrEmpty(n.Text) ? -1 : int.Parse(n.Text))
        .ToList();
    }

    public void ClearAll()
    {
      foreach (var textBox in _partsTextBoxes)
      {
        textBox.Text = String.Empty;
      }
    }

    public void SwapPartState(string value)
    {
      //disable if this value already selected
      if (_partsTextBoxes.Any(p => p.Text == value))
      {
        _partsTextBoxes.First(p => p.Text == value).Text = string.Empty;
        return;
      }

      //select if there is empty place for new value
      var nextPart = _partsTextBoxes.FirstOrDefault(p => p.Text == string.Empty);
      if (nextPart == null)
      {
        return;
      }
      nextPart.Text = value;
    }

    private static TextBox MakeDefaultTextBox(KeyEventHandler valueChanged)
    {
      var tb = new TextBox
      {
        Text = ""
      };
      tb.KeyUp += valueChanged;
      tb.KeyPress += (s, e) =>
      {
        if (!char.IsDigit(e.KeyChar) && e.KeyChar != '\b')
        {
          e.Handled = true;
          return;
        }

        var oldText = (s as TextBox).Text;
        var curValue = e.KeyChar == '\b'
          ? oldText.Substring(0, oldText.Length - 1)
          : oldText + e.KeyChar;

        var maxCoord = (StaticData.TableColumns - 1) * 10 + StaticData.TableRows - 1;
        if (int.TryParse(curValue, out var value) && (value < 0 || value > maxCoord))
        {
          e.Handled = true;
        }
      };

      return tb;
    }

    private static Label MakeDefaultLabel(string text)
    {
      return new Label
      {
        Text = text,
        TextAlign = ContentAlignment.MiddleCenter
      };
    }
  }
}