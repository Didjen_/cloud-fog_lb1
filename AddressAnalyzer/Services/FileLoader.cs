using System.Collections.Generic;
using System.IO;
using AddressAnalyzer.Models;
using Newtonsoft.Json;

namespace AddressAnalyzer.Services
{
  public static class FileLoader
  {
    public static List<AnalyzeModel> LoadFromFile(string path)
    {
      var json = File.ReadAllText(path);
      return JsonConvert.DeserializeObject<List<AnalyzeModel>>(json);
    }

    public static void ExportToFile(string path, object data)
    {
      var json = JsonConvert.SerializeObject(data);
      File.WriteAllText(path, json);
    }
  }
}