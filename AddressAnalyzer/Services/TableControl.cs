using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using AddressAnalyzer.Models;

namespace AddressAnalyzer.Services
{
  public class TableControl
  {
    public Label[,] Table { get; set; } = new Label[StaticData.TableRows, StaticData.TableColumns];

    public void InitTable(TableLayoutPanel tlp, EventHandler onLabelClick)
    {
      for (var i = 0; i < StaticData.TableRows; i++)
      {
        for (var j = 0; j < StaticData.TableColumns; j++)
        {
          var label = MakeDefaultLabel(onLabelClick);
          label.Text = $"{i}{j}";
          tlp.Controls.Add(label, j, i);
          Table[i, j] = label;
        }
      }
    }

    public void ClearGridAndSelectCellsAtPosition(Point[] positions)
    {
      positions = positions.Where(IsInTable).ToArray();

      foreach (var lable in Table)
      {
        lable.BackColor = StaticData.DefaultColor;
      }

      foreach (var position in positions)
      {
        Table[position.X, position.Y].BackColor = StaticData.SelectedColor;
      }
    }

    private static Label MakeDefaultLabel(EventHandler onClick)
    {
      var label = new Label
      {
        TextAlign = ContentAlignment.MiddleCenter,
        BackColor = StaticData.DefaultColor
      };
      label.Click += onClick;
      return label;
    }

    private bool IsInTable(Point p)
    {
      return p.X >= 0 && p.X < Table.GetLength(0) && p.Y >= 0 && p.Y < Table.GetLength(1);
    }
  }
}